//题目分析:水题不分析

//题目网址:http://soj.me/4315

#include <iostream>

using namespace std;

int num[10] = {0};

int main()
{
    int cases;
    int i, k;
    int number;
    cin >> cases;
    while (cases--) {
        cin >> number;
        for(i = 1; i <= number; i++) {
            k = i;
            while (true) {
                num[k%10]++;
                k /= 10;
                if (k == 0) {
                    break;
                }
            }
        }

        for (i = 0; i < 10; i++) {
            if (i == 9) {
                cout << num[i] << endl;
                num[i] = 0;
            } else {
                cout << num[i] << ' ';
                num[i] = 0;
            }
        }
    }
    
    return 0;
}                                 